# リンク集

## 公式ドキュメント

### [Docs - Windows IoT](https://developer.microsoft.com/en-us/windows/iot/Docs)

## 入門/インストール関係

### [Raspberry Pi 2電子工作をWindows 10 IoT Coreで始めよう！ - Build Insider](http://www.buildinsider.net/small/windowsiot/01)

> ついにプレビュー公開が開始されたWindows 10ベースのIoT向けOSは、どのような機能・特徴を持つのか？ラズパイ2にインストールして試してみよう。

### [Downloads - Windows IoT](https://developer.microsoft.com/en-us/windows/iot/downloads)

> Download Windows for IoT, Visual Studio, Software Development Kits or any of the other tools available on this page to get started developing for the Internet of Things today!

## サンプルプロジェクト

### [Microsoft Projects](https://microsoft.hackster.io/en-US)

### [ms-iot/samples: Windows 10 IoT Core Samples](https://github.com/ms-iot/samples)

## UWP関係

### [特集：次期Visual Studioの全貌を探る：徹底予習！ Windows 10のユニバーサルアプリ開発 (1/6) - ＠IT](http://www.atmarkit.co.jp/ait/articles/1504/28/news015.html)

> 本稿では、最近のイベントにおけるマイクロソフトの発表内容やVisual Studio 2015のプレビュー版などから、Windows 10での新しいユニバーサルアプリ開発の概要について解説する。最初に、Windows 10用のユニバーサルアプリとはどのようなものかを説明し、それから開発の概要や新しいAPIについて紹介していく。最後に、既存のアプリをどうすべきか、いくつか指針を示そうと思う。

## Remote Client

### [Windows IoT Remote Client – Microsoft ストアの Windows アプリ](https://www.microsoft.com/ja-jp/store/apps/windows-iot-remote-client/9nblggh5mnxz#app-details)

> The Windows IoT Remote Client application is a part of a remote display and sensor technology available for the Insider build of Windows 10 IoT Core. With a device running the latest Insider build of IoT Core, and a companion Windows 10 device running this application, you can connect the two devices. The Windows 10 IoT Core device will transmit the UI of its UWP app to the companion device, while receiving input and sensor data in return.

### [Remote Display Experience - Windows IoT](https://developer.microsoft.com/en-us/windows/iot/win10/remotedisplay)

> The remote display experience is a technology used to remotely control UWP applications running on a Windows 10 IoT Core device. Remote control can be established from any Windows 10 desktop PC, tablet, or phone, putting a display on every displayless device.


## バッググラウンドプロセスについて

### [Background Applications - Windows IoT](https://developer.microsoft.com/en-us/windows/iot/win10/backgroundapplications)

> Background Applications are applications that have no direct UI. Once deployed and configured, these applications launch at machine startup and run continuously without any process lifetime management resource use limitations. If they crash or exit the system will automatically restart them. These Background Applications have a very simple execution model. The templates create a class that implements the “IBackgroundTask” interface and generates the empty “Run” method. This “Run” method is the entry point to your application.


## デバイスポータルについて

### [Windows Device Portal - Windows IoT](https://developer.microsoft.com/en-us/windows/iot/win10/tools/deviceportal)

> Windows Device Portal provides basic configuration and device management capabilities, in addition to advanced diagnostic tools to help you troubleshoot and view the real time performance of your Windows IoT Device.


## SQLiteを使う

### [Using SQLite in Windows 10 Universal apps](http://igrali.com/2015/05/01/using-sqlite-in-windows-10-universal-apps/)

> Even though Entity Framework 7 support for Windows 10 Universal apps is almost here, you still might decide to just continue using a lighter SQLite.Net-PCL library that you're used to since Windows Phone 8/WinRT.


## サーバー

### [Blinky Webserver - Hackster.io](https://www.hackster.io/windowsiot/blinky-webserver-sample-44ea34)

> We’ll create a simple Blinky app controlled by another app’s WebServer and connect an LED to your Windows IoT Core device. 


